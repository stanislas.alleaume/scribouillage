#!/bin/bash

GREPSTRING=$1

ids=$(ps aux | grep -i $GREPSTRING |awk -F' ' '{ print $2 }')

for id in $ids; do
        echo "---> kill -9 $id"
        kill -9 $id
done
