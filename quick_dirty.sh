#!/bin/bash

#### FONCTION ####

lancerDe() {
    lancer_de=$(( ( RANDOM % 6 )  + 1 ))
    echo "Le lancement du dé à donné ${lancer_de} !"
}

#### SCRIPT ####


echo "lancement du dé ! intensif, maintiens ou repos ? , le dé roule ....."
#lancer_de=$(( ( RANDOM % 6 )  + 1 ))
#sleep 3
#echo "Le lancement du dé à donné ${lancer_de} !"
lancerDe


if [ ${lancer_de} -eq 1 ] 
then
    echo "-> jour de repos"
    exit 0
elif [ $(expr ${lancer_de} % 2) -eq 0 ]
then
    OBJECTIF1="INTENSIF"
    echo "-> Nombre pair : ${OBJECTIF1}"
else
    OBJECTIF1="MAINTIEN"
    echo "-> Nombre impair : ${OBJECTIF1}"
fi

echo "Nouveau lancement de dé ... pair = ENDURANCE - impair = FORCE"
lancerDe
#lancer_de=$(( ( RANDOM % 6 )  + 1 ))
##sleep 2
#echo "Le lancement du dé à donné ${lancer_de} ! "

if [ $(expr ${lancer_de} % 2) -eq 0 ]
then
    OBJECTIF2="ENDURANCE"
    echo "--> Nombre pair : ${OBJECTIF2}"
    echo "déterminons maintenant quel exercice"
    lancerDe
    if [ ${lancer_de} -eq 1 ]
    then
        EXERCICE="UNE GRANDE MARCHE"
        echo "---> Exercice = ${EXERCICE}"
    elif [ ${lancer_de} -eq 2 ]
    then
        EXERCICE="DES 'UPS AND DOWNS'"
        echo "---> Exercice = ${EXERCICE}"
    elif [ ${lancer_de} -eq 3 ]
    then
        EXERCICE="FARTLEK"
        echo "---> Exercice = ${EXERCICE}"
    elif [ ${lancer_de} -eq 4 ]
    then
        EXERCICE="DES TABATAS"
        echo "---> Exercice = ${EXERCICE}"
    elif [ ${lancer_de} -eq 5 ]
    then
        EXERCICE="LE GROS SAC"
        echo "---> Exercice = ${EXERCICE}"
    elif [ ${lancer_de} -eq 6 ]
    then
        EXERCICE="UNE GRANDE MARCHE"
        echo "---> Exercice = ${EXERCICE}"
    else
        echo "ERROR"
    fi

else
    OBJECTIF2="FORCE"
    echo "--> Nombre impair : ${OBJECTIF2}"
    echo "déterminons maintenant quel exercice"
    lancerDe
    if [ ${lancer_de} -eq 1 ]
    then
        EXERCICE="Goblet squats et presse"
        echo "---> Exercice = ${EXERCICE}"

    elif [ ${lancer_de} -eq 2 ]
    then
        EXERCICE="Swings et get-ups"
        echo "---> Exercice = ${EXERCICE}"

    elif [ ${lancer_de} -eq 3 ]
    then
        EXERCICE="Floor press et snatches"
        echo "---> Exercice = ${EXERCICE}"

    elif [ ${lancer_de} -eq 4 ]
    then
        EXERCICE="Tractions et presses"
        echo "---> Exercice = ${EXERCICE}"

    elif [ ${lancer_de} -eq 5 ]
    then
        EXERCICE="Swings et Pompes"
        echo "---> Exercice = ${EXERCICE}"

    elif [ ${lancer_de} -eq 6 ]
    then
        EXERCICE="Goblet squats, presses, tractions & floor press !!!"
        echo "---> Exercice = ${EXERCICE}"

    else
        echo "ERROR"
    
    fi

    ### DEFINIR LE NOMBRE DE SERIES
    lancerDe
    if [ ${lancer_de} -le 3 ]
    then
        SERIES="3x"
        echo "---> Exercice = ${SERIES}"

    elif [ ${lancer_de} -eq 4 ]
    then
        SERIES="4x"
        echo "---> Exercice = ${SERIES}"

    elif [ ${lancer_de} -eq 5 ]
    then
        SERIES="5x"
        echo "---> Exercice = ${SERIES}"

    elif [ ${lancer_de} -eq 6 ]
    then
        SERIES="6x"
        echo "---> Exercice = ${SERIES}"

    else
        echo "ERROR"
    
    fi
fi

echo "---------------------------------------------------------------"
echo "LES OBJECTIFS DU JOUR SONT : ${OBJECTIF1} en ${OBJECTIF2}"
echo "EXERCICE A EFFECTUER : ${EXERCICE} ${SERIES}"
echo "---------------------------------------------------------------"